# TODOAppAPI
### A ToDO API made using Django's REST Framework and Python

## What I learned?
#### * Using Django's models
#### * Using the REST-framework of Django
#### * Using classes in Python to create Django models
#### * Filtering and querying databases in Django and Python
#### * Using loops in HTML5
#### * Updating databases by the use of POST and PATCH requests





Bhavya Muni
Student - Thakur Internation School (Cambridge)